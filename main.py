import sys
import time

from collections import defaultdict
from requests import HTTPError
import statistics

from utils import get_statements, save_results

def extract_date_from_timestamp(timestamp):
    date = timestamp.split("T")[0]  # "2023-04-28"
    return date

def group_statements(statements):
    grouped = defaultdict(lambda: defaultdict(int))
    for statement in statements:
        timestamp = statement["timestamp"]
        day = extract_date_from_timestamp(timestamp)
        verb = statement["verb"]['id'].split('/')[-1]

        grouped[day][verb]+=1
    return grouped

def main(analytics_token, api_url):
    try:
        # Retrieve available statements
        statements = get_statements(api_url, analytics_token)
        grouped_statements = group_statements(statements)


        description = {
            "de": "Anzahl jedes einzelnen verbs pro tag",
            "en": "Frequency of each individual verb per day",
        }

        # Send result to rights engine
        result_time = int(time.time())
        save_results(
            api_url,
            analytics_token,
            {'time' : result_time,"result": grouped_statements, "description": description},
        )
    except HTTPError as error:
        print(error.response.status_code, error.response.text)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])